
from rest_framework import serializers
from quickstart.models import Product


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['product_id', 'sku', 'name', 'image_name']


class CartSerializer(serializers.Serializer):
    product_id = serializers.IntegerField()
    product_link = serializers.CharField()
    name = serializers.CharField()
    image_name = serializers.CharField()
    sku = serializers.CharField()
    price_excluding_tax = serializers.FloatField()
    price_including_tax = serializers.FloatField()
