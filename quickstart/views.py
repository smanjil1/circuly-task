
import requests

from django.contrib.sessions.models import Session
from django.conf import settings
from rest_framework import viewsets, status
from rest_framework import permissions
from rest_framework.response import Response
from quickstart.serializers import ProductSerializer, CartSerializer
from quickstart.models import Product
from quickstart.utils import create_cart_item, convert_session_data_type


class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows products to be viewed
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]


class CartViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows carts to be viewed
    """
    queryset = Session.objects.all()
    serializer_class = CartSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        """Endpoint to create empty cart (POST)

        args:
            request: request data
            *args: any list items
            **kwargs: {key: val} items provided as json in request
        """
        try:
            self.queryset.create(
                session_key=request.user.username,
                session_data=[],
                expire_date='2020-10-02 16:12:40.059308+02'
            )

            return Response(
                self.queryset.get(session_key=request.user).session_data,
                status=status.HTTP_201_CREATED)
        except Exception:
            return Response('You already have a cart!',
                            status=status.HTTP_403_FORBIDDEN)

    def list(self, request, *args, **kwargs):
        """Get request to list all cart items (GET) request"""
        cart_items = convert_session_data_type(self.queryset, request.user)
        res = CartSerializer(cart_items.session_data, many=True).data

        return Response(res, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        """Endpoint to update empty cart (PUT)

        args:
            request: request data, items to be updated as json
            *args: any list items
            **kwargs: {key: val} items provided as json in request,
            id to update
        """
        cart_item_id = int(kwargs['pk'])

        json_data = requests.get(
            "https://lpsuitetest1.wiwwo.com/json/product_response.json").json()

        product_id = json_data['data']['id']
        product_data = Product.objects.get(product_id=product_id)

        qs = convert_session_data_type(self.queryset, request.user)

        all_product_ids = [val['product_id'] for val in
                           qs.session_data]

        if cart_item_id not in all_product_ids:
            session_data_val = create_cart_item(cart_item_id,
                                                settings.PRODUCT_URL,
                                                product_data.id,
                                                json_data)

            qs.session_data.append(session_data_val)
        else:
            for val in qs.session_data:
                if val['product_id'] == cart_item_id:
                    for item in request.data:
                        val[item] = request.data[item]

        qs.save()

        res = CartSerializer(qs.session_data, many=True).data

        return Response(
            res,
            status=status.HTTP_201_CREATED
        )

    def destroy(self, request, *args, **kwargs):
        """Delete cart items (DELETE) request"""
        cart_item_id = int(kwargs['pk'])
        qs = convert_session_data_type(self.queryset, request.user)

        for i, val in enumerate(qs.session_data):
            if val['product_id'] == cart_item_id:
                index_to_be_deleted = i

                qs.session_data.pop(index_to_be_deleted)
            else:
                print(f"Cart item {cart_item_id} is already deleted.")

        qs.save()

        res = CartSerializer(qs.session_data, many=True).data

        return Response(res,
                        status=status.HTTP_201_CREATED)
