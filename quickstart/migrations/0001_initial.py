# flake8: noqa
# Generated by Django 3.1 on 2020-09-18 15:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_id', models.PositiveIntegerField(unique=True)),
                ('name', models.CharField(default='', max_length=256)),
                ('description', models.CharField(max_length=512)),
                ('added', models.DateTimeField()),
                ('tax_id', models.PositiveIntegerField()),
                ('tax_amount', models.FloatField()),
                ('image_name', models.CharField(default='', max_length=256)),
                ('sku', models.CharField(max_length=64)),
                ('price', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sku', models.CharField(blank=True, max_length=64)),
                ('name', models.CharField(default='', max_length=256)),
                ('price_excluding_tax', models.FloatField(blank=True)),
                ('image_name', models.CharField(default='', max_length=256)),
                ('price_including_tax', models.FloatField(blank=True)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quickstart.product')),
            ],
        ),
    ]
