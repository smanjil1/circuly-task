# flake8: noqa
# Generated by Django 3.1 on 2020-09-19 15:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quickstart', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Cart',
        ),
    ]
