
import logging

from quickstart.models import Product

logger = logging.getLogger(__name__)


def import_product(json_data):
    """Import product data to Produt model

    Args:
        json_data: json data response.
    """
    product_id = json_data['data']['id']
    name = json_data['data']['name']
    description = json_data['data']['description']
    added = json_data['data']['added']
    tax_id = json_data['data']['taxId']
    tax_amount = json_data['data']['tax']['tax']
    image_name = (f"{json_data['data']['images'][0]['path']}."
                  f"{json_data['data']['images'][0]['extension']}")
    sku = json_data['data']['mainDetail']['number']
    price = json_data['data']['mainDetail']['prices'][0]['price']

    Product.objects.create(
        product_id=product_id,
        name=name,
        description=description,
        added=added,
        tax_id=tax_id,
        tax_amount=tax_amount,
        image_name=image_name,
        sku=sku,
        price=price
    )
