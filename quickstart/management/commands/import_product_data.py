
import logging
import requests

from django.core.management.base import BaseCommand, CommandError
from quickstart.management.commands.utils.save_product import import_product

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """Manager command to import product data"""

    help = 'Import product data to database.'

    def add_arguments(self, parser):
        """Cli argument handler.

        Args:
            parser: Argument parser
        """
        parser.add_argument('product_json_path', nargs='+', type=str)

    def handle(self, *args, **options):
        """Command handler.

        Args:
            **args: Cli arguments
            **options: Option handlers
        """
        logger.info(f"Json file {options['product_json_path']}")
        fpath = options['product_json_path'][0]

        try:
            json_data = requests.get(fpath).json()
        except Exception:
            logger.error(f"{fpath} does not exist")
            raise CommandError(f"File {fpath} does not exist.")

        logger.info("Starting import of product data")
        import_product(json_data)
