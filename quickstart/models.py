from django.db import models


class Product(models.Model):
    product_id = models.PositiveIntegerField(unique=True)
    name = models.CharField(max_length=256, default='')
    description = models.CharField(max_length=512)
    added = models.DateTimeField()
    tax_id = models.PositiveIntegerField()
    tax_amount = models.FloatField()
    image_name = models.CharField(max_length=256, default='')
    sku = models.CharField(max_length=64)
    price = models.FloatField()
