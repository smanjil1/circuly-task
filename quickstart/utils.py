
def create_cart_item(cart_item_id, product_url, pid, json_data):
    product_id = cart_item_id
    product_link = f"{product_url}/{pid}"
    name = json_data['data']['name']
    image_name = (f"{json_data['data']['images'][0]['path']}."
                  f"{json_data['data']['images'][0]['extension']}")
    sku = json_data['data']['mainDetail']['number']
    price_excluding_tax = float(json_data['data']['mainDetail'][
                                    'prices'][0]['price'])
    tax_amount = float(json_data['data']['tax']['tax'])
    price_including_tax = (price_excluding_tax * tax_amount) + \
        price_excluding_tax

    session_data_val = {
        'product_id': product_id,
        'product_link': product_link,
        'name': name,
        'image_name': image_name,
        'sku': sku,
        'price_excluding_tax': price_excluding_tax,
        'price_including_tax': price_including_tax
    }

    return session_data_val


def convert_session_data_type(qs, user):
    qss = qs.get(session_key=user)
    qss.session_data = eval(qss.session_data)

    return qss
