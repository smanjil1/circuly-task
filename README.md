## Circuly task for rest api

### Install dependencies
```
$ pipenv shell --python=/usr/bin/python3.8
$ pipenv install
```

### This is already a built Django project, so only have to perform:
```
$ cd circuly-task
$ python manage.py makemigrations (prepares sql schema for the models built)
$ python manage.py migrate (migrates the schema and creates table inside the chosen database)
$ python manage.py createsuperuser (creates admin user)

-- I have checked in the migrations file also, so it is now only necessary to perform
$ python manage.py migrate
```

### For a short example, I created a Product table(model) in Django, so I did migrate a minimal project using the json response:
```
$ python manage.py import_product_data https://lpsuitetest1.wiwwo.com/json/product_response.json
```

### Lets us run the server
-- DOCKER = False
```
$ python manage.py runserver

-- The server will be available at:
    127.0.0.1:8000
```

### If we look at it, we will get:
![Home page](readme_images/homepage.png)


### Clicking at any link will give
![Auth error](readme_images/auth-error.png)


### Solving this:
```
1. Login using the superuser created above (python manage.py createsuperuser)
2. Creating a auth token
```


### Creating auth token
```
$ python manage.py drf_create_token <username from above>

This will display a token in the console for the user.
This can hence be used to invoke the api using token.
```


### Let us try invoking some api (I am using http here.)
```
-- Listing products (I did create a small database for this purpose)
$ http http://127.0.0.1:8000/product/ 'Authorization: Token <token generated above>'

-- Creating a empty cart (return empty list as a initilization for session data)
$ http -f POST http://127.0.0.1:8000/cart/ 'Authorization: Token <token generated above>'

-- Updating the cart item if not already present for the product (Returns the items in cart as json)
$ http -f PUT http://127.0.0.1:8000/cart/239/ 'Authorization: Token <token generated above>' <<< '{"product_id": 239, "sku": "SW10230", "name": Dart Automat", "image_name": "Turnier-Dart-Automat.jpg"}'
-- 239 is the product id which we want to add to the cart.
-- The script will then also fetch the actual product id from the db to create a link.

-- Updating the existing cart item
$ http PUT http://127.0.0.1:8000/cart/240/ 'Authorization: Token <token generated above>' <<< '{"name": "testy"}'

-- Listing all cart items
$ http http://127.0.0.1:8000/cart/ 'Authorization: Token <token generated above>'

-- Deleting a cart item
$ http DELETE http://127.0.0.1:8000/cart/239/ 'Authorization: Token <token generated above>'
-- This will remove this(product id 239) item from the cart but not the db itself.
```

### Sample json response from the get or update request:
![Response](readme_images/response.png)

### This response can be directly used in the frontend side by using json.loads or similar command.

### Dockerized version
-- DOCKER = True (in settings.py)

```
$ docker-compose up -d --build
- This will start two containers(POSTGRES and Django project)
```


### Now, we have the api service running inside docker container
#### Here, we can perform the same things as done above
```
$ docker-compose run web python /code/manage.py makemigrations
$ docker-compose run web python /code/manage.py migrate
$ docker-compose run web python /code/manage.py createsuperuser
$ docker-compose run web python /code/manage.py drf_create_token <username>
$ docker-compose run web python /code/manage.py runserver
$ docker-compose run web python /code/manage.py import_product_data https://lpsuitetest1.wiwwo.com/json/product_response.json
```

#### Accessing api
```
$ http http://127.0.0.1:8000/product/ 'Authorization: Token <token generated above>'
$ http POST http://127.0.0.1:8000/cart/ 'Authorization: Token <token generated above>'
$ http -f PUT http://127.0.0.1:8000/cart/239/ 'Authorization: Token <token generated above>' <<< '{"product_id": 239, "sku": "SW10230", "name": Dart Automat", "image_name": "Turnier-Dart-Automat.jpg"}'
$ http PUT http://127.0.0.1:8000/cart/239/  'Authorization: Token <token generated above>' <<< '{"name": "testy"}'
$ http DELETE http://127.0.0.1:8000/cart/239/ 'Authorization: Token <token generated above>'
```